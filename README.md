## State of the Art
* [Deep Index](https://deepindex.org/)
* [Trending Papers With Code](https://paperswithcode.com/)
* [State of AI Report](https://www.stateof.ai/)

# AI-Resources
* [Repo of Artificial Intelligence courses, books, video lectures and papers.](https://github.com/brainpool-ai/AI-Resources/blob/master/AI%20resources.md)
* [Books](https://github.com/brainpool-ai/AI-Resources/blob/master/AI%20resources.md#books)
* [Online Courses and vedios](https://github.com/brainpool-ai/AI-Resources/blob/master/AI%20resources.md#online-courses)
* [Tools](https://github.com/brainpool-ai/AI-Resources/blob/master/AI%20resources.md#tools)

## Machine Learning
* [Repo of useful Machine Learning frameworks, libraries and software](https://github.com/brainpool-ai/AI-Resources/blob/master/Machine%20Learning%20Sources.md)

## ChatBot
* [Research Paper notes with images](https://github.com/ricsinaruto/Seq2seqChatbots/wiki/Chatbot-and-Related-Research-Paper-Notes-with-Images?utm_source=share&utm_medium=ios_app)

## NLP
* [Repo of NLP Courses, Tutorials, Libraries, Datasets.](https://github.com/brainpool-ai/AI-Resources/blob/master/NLP.md)
* [Repo of Speech and NLP resources](https://github.com/brainpool-ai/AI-Resources/blob/master/Speech%20and%20NLP.md)
* [List of NLP libraries](https://github.com/brainpool-ai/AI-Resources/blob/master/NLP%20libraries.md)

## Computer Vision
* [Books](https://github.com/brainpool-ai/AI-Resources/blob/master/Computer%20Vision.md#books)
* [Courses](https://github.com/brainpool-ai/AI-Resources/blob/master/Computer%20Vision.md#courses)
* [Papers](https://github.com/brainpool-ai/AI-Resources/blob/master/Computer%20Vision.md#papers)
* [Software tools](https://github.com/brainpool-ai/AI-Resources/blob/master/Computer%20Vision.md#software)
* [Datasets](https://github.com/brainpool-ai/AI-Resources/blob/master/Computer%20Vision.md#datasets)

## Reinforcement Learning
* [John Schulman's Deep Reinforcement Learning lectures for MLSS 2016 in Cadiz.](https://frnsys.com/ai_notes/scratch/deep_rl.html)

## Infrastructure
* [Pipeline AI](https://github.com/PipelineAI/pipeline)

## Podcasts
* [Lex Fridman](https://www.youtube.com/channel/UCSHZKyawb77ixDdsGog4iWA)
